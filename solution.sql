CREATE DATABASE enrollment_db;

USE enrollment_db;

CREATE TABLE tbl_students(
    id INT NOT NULL AUTO_INCREMENT,
    student_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE tbl_teachers(
    id INT NOT NULL AUTO_INCREMENT,
    teacher_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE tbl_courses(
    id INT NOT NULL AUTO_INCREMENT,
    course_name VARCHAR(255) NOT NULL,
    teacher_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_courses_teachers_id
    FOREIGN KEY(teacher_id) REFERENCES tbl_teachers(id)
);

CREATE TABLE tbl_student_courses(
    id INT NOT NULL AUTO_INCREMENT,
    course_id INT NOT NULL,
    student_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_student_courses_course_id
    FOREIGN KEY(course_id) REFERENCES tbl_courses(id),
    CONSTRAINT fk_student_courses_student_id
    FOREIGN KEY(course_id) REFERENCES tbl_students(id)    
);